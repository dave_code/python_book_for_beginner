data_total = [8, 2, 4, 11, 1, 9]

def quick_sort(data):
    if len(data) < 1:
        return data
    else:
        pivot = data[0]
        less, equal, more = [], [], []
        for index in range(len(data)):
            if pivot > data[index]:
                less.append(data[index])
            elif pivot < data[index]:
                more.append(data[index])
            else:
                equal.append(data[index])

    return quick_sort(less) + equal + quick_sort(more)

print(quick_sort(data_total))






