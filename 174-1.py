
class vending:
    # 책에 관련 부분에 대한 설명이 빠져 있습니다.
    # def __init__(self) 함수는 최초 클래스가 객체로 만들어질 때 호출되는 함수입니다.
    # 해당 함수 안에서 클래스 내부 변수를 선언하고, 초기화해야 동일 클래스로 여러 객체를 만들시
    # 각 객체 내부 변수가 중첩되지 않고, 개별적으로 데이터가 저장될 수 있습니다.
    def __init__(self):
        self.location = ""
        self.drink_data = {}

    def calculation(self, selected_drink_name, payment):
        # 사전키에 해당 키가 들어 있는지 확인하기 위한 방법으로 다음과 같은 문법을 사용할 수 있습니다.
        if selected_drink_name in self.drink_data.keys():
            print(payment - self.drink_data[selected_drink_name])
        else:
            print("Not provided")

vending1 = vending()
vending2 = vending()
vending3 = vending()

vending1.location = "seoul"
vending2.location = "incheon"
vending3.location = "suwon"

vending1.drink_data["cola"] = 1000
vending1.drink_data["juice"] = 1500

vending2.drink_data["water"] = 900
vending2.drink_data["sprite"] = 1100

vending3.drink_data["milk"] = 500

vending_name = input("vending name:")
selected_drink_name = input("drink name:")
payment = input("payment:")

if vending_name == vending1.location:
    vending1.calculation(selected_drink_name, int(payment))
elif vending_name == vending2.location:
    vending2.calculation(selected_drink_name, int(payment))
elif vending_name == vending3.location:
    vending3.calculation(selected_drink_name, int(payment))

