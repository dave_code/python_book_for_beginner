data_total = [19, 80, 77, 11, 54]

repeat_count = 0
changed = True
while changed:
    changed = False
    repeat_count += 1
    for index in range(len(data_total) - repeat_count):
        if data_total[index] > data_total[index+1]:
            data_total[index], data_total[index+1] = data_total[index+1], data_total[index]
        changed = True

print(data_total)
