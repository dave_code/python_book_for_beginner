data_list = [1, 9, 2, 3, 7, 5, 4, 7, 10, 11, 23, 43, 54, 54, 32, 1, 2, 34, 3, 2, 5, 6, 6, 9, 53, 19, 18]

for count in range(len(data_list) - 1):
    min_index = count
    for index in range(count, len(data_list)):
        if data_list[index] < data_list[min_index]:
            min_index = index
    data_list[count], data_list[min_index] = data_list[min_index], data_list[count]

print(data_list)

