
population = [9860,3400,2455,2886,1517,1536,1142,197,12398,1506,1561,2089,1798,1757,2642,3285]
print(sum(population)) #총인구
print(round(sum(population) / len(population))) #평균



population_center = {"서울":9860, "인천":2886, "경기":12398}
print(sum(population_center.values())) #수도권 인구 합계
print(round(sum(population_center.values()) / len(population_center.values()))) #수도권 인구 평균


population2 = {"서울":9860, "부산":3400, "대구":2455, "인천":2886, "광주":1517, "대전":1536, "울산":1142, "세종":197, "경기":12398, "강원":1506, "충북":1561, "충남":2089, "전북":1798, "전남":1757, "경북":2642, "경남":3285}
population2.update({"제주":587})
population2.update({"서울":10200})
population2.update({"인천":3200})
population2.update({"경기":10300})

print(population2) #수정된 사전변수값
print(sum(population2.values())) #수정된 인구합계
print(round(sum(population2.values()) / len(population2.values()))) #수정된 인구평균
