
for count in range(10):
    # 입력
    user_data1 = input()
    user_data2 = input()
    user_opt = input()

    # 계산
    if user_opt == '+':
        user_total = int(user_data1) + int(user_data2)
        print(user_total)
    elif user_opt == '-':
        user_total = int(user_data1) - int(user_data2)
        print(user_total)
    elif user_opt == '*':
        user_total = int(user_data1) * int(user_data2)
        print(user_total)
    elif user_opt == '/':
        user_total = int(user_data1) / int(user_data2)
        print(user_total)
    elif user_opt == '%':
        user_total = int(user_data1) % int(user_data2)
        print(user_total)
