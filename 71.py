def fact(digit):
    if digit == 1:
        return 1
    else:
        return digit * fact(digit - 1)

print(fact(4))