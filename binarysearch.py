data = [2, 5, 6, 9, 12, 15, 16, 19, 24, 27, 30]

def binary_search(list, item):
     low = 0
     high = len(list) - 1

     while low <= high:
         mid = (low + high) // 2
         if list[mid] < item:
             low = mid + 1
         elif list[mid] > item:
             high = mid - 1
         else:
             return mid

print(binary_search(data, 15))